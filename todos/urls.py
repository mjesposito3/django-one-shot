from django.urls import path

from todos.views import (
    TodoListListView,
    TodoListCreateView,
    TodoListDetailView,
    TodoListDeleteView,
    TodoListUpdateView,
    TodoItemUpdateView,
    TodoItemCreateView,
)

urlpatterns = [
    path("", TodoListListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_list_delete"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todo_list_update"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create"),
    path("items/create/", TodoItemCreateView.as_view(), name="todo_item_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todo_item_update"),
]
